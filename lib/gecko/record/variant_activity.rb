require 'gecko/record/base'

module Gecko
  module Record
    class VariantActivity < Base

      attribute :id,          Integer
      attribute :user_id,     Integer
      attribute :variant_id,  Integer
      attribute :quantity,    String
      attribute :reason,      String
      attribute :direction,   String
      attribute :note,        String
      attribute :published,   DateTime
      attribute :cost_price,  String
      attribute :company,     String
      attribute :number,      String
      attribute :location,    String
      attribute :source,      String
      attribute :actor,       Hash

      attribute :object, Hash

      attribute :verb, String
      attribute :target, Hash

      def sign_quantity
        if direction == "increase"
          quantity
        elsif direction == "decrease"
          quantity.to_f * -1
        else
          quantity
        end

      end

    end
    class VariantActivityAdapter < BaseAdapter

    end
  end
end
